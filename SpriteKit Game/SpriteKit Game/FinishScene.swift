//
//  FinishScene.swift
//  SpriteKit Game
//
//  Created by Swift_entiwckler on 09.04.19.
//  Copyright © 2019 DHBW_Game_Production. All rights reserved.
//

import Foundation
import SpriteKit

class FinishScene: SKScene {
    
    //Score variables
    var timeScore: TimeInterval = 0.0
    private let HIGHSCORE_KEY = "flatracer_highscore"

    //Format for timer
    private let minuteFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.minimumIntegerDigits = 2
        return formatter
    }()
    
    private let nanosecondFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.minimumIntegerDigits = 4
        return formatter
    }()

    // MARK: Load and Save Highscore
    func loadHighscore() -> TimeInterval {
        let defaults: UserDefaults = UserDefaults.standard
        if let loadedHighScoreString = defaults.value(forKey: HIGHSCORE_KEY) as? String {
            if let loadedHighScore = TimeInterval(loadedHighScoreString) {
                return loadedHighScore
            }
        }
        //Return -1 if the highscore could not be loaded
        return -1
    }

    func saveHighscore() {
        let highScoreString = String(format:"%f", timeScore)
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(highScoreString, forKey: HIGHSCORE_KEY)
        defaults.synchronize()
    
    }
    
    // MARK: Timer function
    func createDateString(from interval: TimeInterval) -> String{
        let minutes = floor(interval/60)
        let minutesString = minuteFormatter.string(from: NSNumber(value: minutes))
        let seconds = Int(floor(interval).truncatingRemainder(dividingBy: 60))
        let secondsString = minuteFormatter.string(from: NSNumber(value: seconds))
        let nanoseconds = Int((interval - floor(interval)) * 10000)
        let nanosecondString = nanosecondFormatter.string(from: NSNumber(value: nanoseconds))
        
        return minutesString! + ":" + secondsString! + ":" + nanosecondString!
    }

    override func didMove(to view: SKView) {
        let timer = childNode(withName: "timer") as? SKLabelNode
        let highScoreNode = childNode(withName: "highscore") as? SKLabelNode
        
        let loadedHighscore = loadHighscore()
        //If the highscore could be loaded
        if loadedHighscore != -1 {
            if loadedHighscore > timeScore {
                highScoreNode?.text = "NEW Highscore: " + createDateString(from: timeScore)
                //Draw the label in a golden Color
                highScoreNode?.fontColor = UIColor(red: 252.0/255.0, green: 194.0/255.0, blue:0, alpha:1.0)
                saveHighscore()
            } else {
                highScoreNode?.text = "Highscore: " + createDateString(from: loadedHighscore)
                highScoreNode?.fontColor = UIColor.white
            }
        } else {
            //No Highscore was stored -> New Highscore
            highScoreNode?.text = "NEW Highscore: " + createDateString(from: timeScore)
            highScoreNode?.fontColor = UIColor(red: 252.0/255.0, green: 194.0/255.0, blue:0, alpha:1.0)
            saveHighscore()
        }
        
        timer?.text = createDateString(from: timeScore)
    }
    
    // MARK: Touch events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: self)
        
        let frontTouchedNode = atPoint(location)
        
        if frontTouchedNode.name == "Retry" {
            
            if view != nil {
                let scaleAction = SKAction.scale(to: 0.8, duration: 0.1)
                frontTouchedNode.run(scaleAction)
                
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: self)
        
        let frontTouchedNode = atPoint(location)
        
        if view != nil {
            let scaleAction = SKAction.scale(to: 1, duration: 0.1)
            if frontTouchedNode.name == "Retry" {
                frontTouchedNode.run(scaleAction, completion: {
                    let transition:SKTransition = SKTransition.doorsOpenHorizontal(withDuration: 1.0)
                    
                    let scene:SKScene = SKScene(fileNamed: "MenuScene")!
                    scene.scaleMode = SKSceneScaleMode.aspectFill
                    self.view?.presentScene(scene, transition: transition)
                })
            } 
        }
    }
}
