//
//  GameScene.swift
//  SpriteKit Game
//
//  Created by Swift_entiwckler on 05.04.19.
//  Copyright © 2019 DHBW_Game_Production. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    // MARK: Node name constants
    private let CAR_NAME = "carBody"
    private let FRONT_WHEEL_NAME = "frontWheel"
    private let REAR_WHEEL_NAME = "rearWheel"
    private let BREAK_PEDAL_NAME = "break"
    private let DRIVE_PEDAL_NAME = "gas"
    private let CAMERA_NAME = "camera"
    private let REWIND_NAME = "rewind"
    private let TIMER_NAME = "timer"
    private let HOMEBUTTON_NAME = "homeButton"
    
    // MARK: Winning constant
    private let WINNING_DISTANCE = 12129
    
    // MARK: Car nodes
    private var car: SKSpriteNode?
    private var frontWheel: SKSpriteNode?
    private var rearWheel: SKSpriteNode?
    
    // MARK: Camera overlay nodes
    private var breakPedal: SKSpriteNode?
    private var drivingPedal: SKSpriteNode?
    private var rewindButton: SKSpriteNode?
    private var homeButton: SKSpriteNode?
    private var timer: SKLabelNode?
    
    //MARK: Particle effect nodes
    private var rainEffect:SKEmitterNode?
    private var snowEffect:SKEmitterNode?
    private var sparkEffect:SKEmitterNode?
    
    // MARK: Car physics parameter
    private let SUSPENSION_DAMPING = CGFloat(1)
    private let SUSPENSION_FREQUENCY = CGFloat(18)
    private let SUSPENSION_DAMPING_HELPER = CGFloat(10)
    private let SUSPENSION_FREQUENCY_HELPER = CGFloat(25)
    
    // MARK: Scene change variable
    private var changingScene = false
    
    // MARK: Time variables
    private var startTime:Date?
    private var currentDateInterval: TimeInterval = 0.0
    private let minuteFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.minimumIntegerDigits = 2
        return formatter
        }()
    
    private let nanosecondFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.minimumIntegerDigits = 4
        return formatter
    }()
    
    // MARK: Initialize Methods
    /* All Nodes that are child nodes of the camera need to be positioned absolute to
     * the screen. Because of using the aspectFill Function, it is necessary to do this
     * manually here
     */
    private func initializeUIElements() {
        let topLeftView = CGPoint(x:scene!.view!.frame.minX,y:scene!.view!.frame.minY)
        let topLeftScene = scene!.view!.convert(topLeftView, to:scene!)
        let topLeftCamera = scene!.convert(topLeftScene,to:camera!)
        
        let bottomRightView = CGPoint(x:scene!.view!.frame.maxX,y:scene!.view!.frame.maxY)
        let bottomRightScene = scene!.view!.convert(bottomRightView, to:scene!)
        let bottomRightCamera = scene!.convert(bottomRightScene,to:camera!)
        
        let SPACING = CGFloat(35.0)
        
        drivingPedal?.position.x = bottomRightCamera.x - 1.33 * drivingPedal!.size.width
        drivingPedal?.position.y = bottomRightCamera.y + drivingPedal!.size.height/3
        
        breakPedal?.position.x = topLeftCamera.x + 1.33 * breakPedal!.size.width
        breakPedal?.position.y = bottomRightCamera.y + breakPedal!.size.height/3
        
        rewindButton?.position.x = bottomRightCamera.x - 1.33 * rewindButton!.size.width
        rewindButton?.position.y = topLeftCamera.y - rewindButton!.size.height/2 - SPACING
        
        homeButton?.position.x = topLeftCamera.x + 1.33 * homeButton!.size.width
        homeButton?.position.y = topLeftCamera.y - homeButton!.size.height / 2 - SPACING
        
        timer?.position.x = -timer!.frame.width / 2
        timer?.position.y = topLeftCamera.y - timer!.frame.height/2 - SPACING
    }
    
    /*
     * Positioning all particle emitters
     */
    func initializeEffects() {
        rainEffect?.position = CGPoint(x: 300, y: 600)
        rainEffect?.zPosition = -1
        addChild(rainEffect!)
        
        snowEffect?.position = CGPoint(x: 2000, y: 600)
        snowEffect?.zPosition = -1
        addChild(snowEffect!)
        
        sparkEffect?.position = CGPoint(x: 12129, y: -176)
        sparkEffect?.zPosition = -1
        addChild(sparkEffect!)
    }
    
    // MARK: Physic
    /* Adjust the physic Body for every Node
     */
    func initializePhysics() {
        if let carPhysicsBody = car!.physicsBody, let frontWheelBody = frontWheel!.physicsBody, let rearWheelBody = rearWheel!.physicsBody {
            
            //The two anchor points for the suspensions
            let carAnchorPointFront = car!.convert(CGPoint(x: 88, y: 10), to: scene!)
            let carAnchorPointFrontHelpingRight = car!.convert(CGPoint(x: 120, y: -30), to: scene!)
            let carAnchorPointFrontHelpingLeft = car!.convert(CGPoint(x: 60, y: -30), to: scene!)
            
            let carAnchorPointRear = car!.convert(CGPoint(x: -92, y: 10), to: scene!)
            let carAnchorPointRearHelpingRight = car!.convert(CGPoint(x: -60, y: -30), to: scene!)
            let carAnchorPointRearHelpingLeft = car!.convert(CGPoint(x: -120, y: -30), to: scene!)
            
            let frontSuspension = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: frontWheelBody, anchorA: carAnchorPointFront, anchorB: frontWheel!.position)
            frontSuspension.damping = SUSPENSION_DAMPING
            frontSuspension.frequency = SUSPENSION_FREQUENCY
            physicsWorld.add(frontSuspension)
            
            let rearSuspension = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: rearWheelBody, anchorA: carAnchorPointRear, anchorB: rearWheel!.position)
            rearSuspension.damping = SUSPENSION_DAMPING
            rearSuspension.frequency = SUSPENSION_FREQUENCY
            physicsWorld.add(rearSuspension)
            
            let frontHelpingSuspensionRight = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: frontWheelBody, anchorA: carAnchorPointFrontHelpingRight, anchorB: frontWheel!.position)
            frontHelpingSuspensionRight.damping = SUSPENSION_DAMPING_HELPER
            frontHelpingSuspensionRight.frequency = SUSPENSION_FREQUENCY_HELPER
            physicsWorld.add(frontHelpingSuspensionRight)
            
            let frontHelpingSuspensionLeft = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: frontWheelBody, anchorA: carAnchorPointFrontHelpingLeft, anchorB: frontWheel!.position)
            frontHelpingSuspensionLeft.damping = SUSPENSION_DAMPING_HELPER
            frontHelpingSuspensionLeft.frequency = SUSPENSION_FREQUENCY_HELPER
            physicsWorld.add(frontHelpingSuspensionLeft)
            
            let rearHelpingSuspensionRight = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: rearWheelBody, anchorA: carAnchorPointRearHelpingRight, anchorB: rearWheel!.position)
            rearHelpingSuspensionRight.damping = SUSPENSION_DAMPING_HELPER
            rearHelpingSuspensionRight.frequency = SUSPENSION_FREQUENCY_HELPER
            physicsWorld.add(rearHelpingSuspensionRight)
            
            let rearHelpingSuspensionLeft = SKPhysicsJointSpring.joint(withBodyA: carPhysicsBody, bodyB: rearWheelBody, anchorA: carAnchorPointRearHelpingLeft, anchorB: rearWheel!.position)
            rearHelpingSuspensionLeft.damping = SUSPENSION_DAMPING_HELPER
            rearHelpingSuspensionLeft.frequency = SUSPENSION_FREQUENCY_HELPER
            physicsWorld.add(rearHelpingSuspensionLeft)
        }
    }
    
    // MARK: Node loading functions
    /*
     * Checks if all Nodes could be loaded
     */
    func checkForAllNodes() -> Bool {
        if car != nil, frontWheel != nil, rearWheel != nil, breakPedal != nil, drivingPedal != nil, camera != nil, rewindButton != nil, homeButton != nil, timer != nil, rainEffect != nil, snowEffect != nil, sparkEffect != nil {
            return true
        } else {
            return false
        }
    }
    
    func loadAllNodes() -> Bool {
        let cameraNode = self.childNode(withName: CAMERA_NAME) as? SKCameraNode
        camera = cameraNode
        
        car = self.childNode(withName: CAR_NAME) as? SKSpriteNode
        frontWheel = self.childNode(withName: FRONT_WHEEL_NAME) as? SKSpriteNode
        rearWheel = self.childNode(withName: REAR_WHEEL_NAME) as? SKSpriteNode
        
        breakPedal = camera!.childNode(withName: BREAK_PEDAL_NAME) as? SKSpriteNode
        drivingPedal = camera!.childNode(withName: DRIVE_PEDAL_NAME) as? SKSpriteNode
        rewindButton = camera!.childNode(withName: REWIND_NAME) as? SKSpriteNode
        homeButton = camera!.childNode(withName: HOMEBUTTON_NAME) as? SKSpriteNode
        timer = camera!.childNode(withName: TIMER_NAME) as? SKLabelNode
        
        rainEffect = SKEmitterNode(fileNamed: "RainEffect.sks")
        snowEffect = SKEmitterNode(fileNamed: "SnowEffect.sks")
        sparkEffect = SKEmitterNode(fileNamed: "SparkEffect.sks")
        
        return checkForAllNodes()
    }
    
    func initializeCameraConstraints() {
        let range = SKRange(upperLimit: 20)
        let cameraConstraintDist = SKConstraint.distance(range, to: car!)
        cameraConstraintDist.enabled = true
        self.camera!.constraints = [cameraConstraintDist]
    }
    
    override func didMove(to view: SKView) {
        if loadAllNodes() {
            initializeUIElements()
            initializeEffects()
            initializePhysics()
            initializeCameraConstraints()
            
        } else {
            //Back to the menu if there was an error...
            moveToMenu()
        }
    }
    
    // MARK: Touch Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: self)
        let frontTouchedNode = atPoint(location)
        
        if !changingScene, view != nil {
            let scaleAction = SKAction.scale(to: 0.8, duration: 0.1)
            if frontTouchedNode.name == DRIVE_PEDAL_NAME {
                
                //Start the Timer when Pedal is touched the first time
                if startTime == nil {
                    startTime = Date()
                }
                frontTouchedNode.run(scaleAction)
                
                let gasAction = SKAction.repeatForever(
                    SKAction.sequence([SKAction.sequence([
                        SKAction.run {
                            if !self.changingScene {
                                self.frontWheel?.physicsBody?.applyAngularImpulse(-5)
                                self.rearWheel?.physicsBody?.applyAngularImpulse(-5)
                            }
                        },
                        SKAction.wait(forDuration: 0.1)
                        ])]))
                
                car?.run(gasAction)
            }
            
            if frontTouchedNode.name == BREAK_PEDAL_NAME {
                if startTime == nil {
                    startTime = Date()
                }
                frontTouchedNode.run(scaleAction)
                let breakAction = SKAction.repeatForever(
                    SKAction.sequence([SKAction.sequence([
                        SKAction.run {
                            if !self.changingScene {
                                self.frontWheel?.physicsBody?.applyAngularImpulse(5)
                                self.rearWheel?.physicsBody?.applyAngularImpulse(5)
                            }
                        },
                        SKAction.wait(forDuration: 0.1)
                        ])]))
                car?.run(breakAction)
            }
            
            if frontTouchedNode.name == REWIND_NAME {
                frontTouchedNode.run(scaleAction)
                reloadScene()
            }
            
            if frontTouchedNode.name == HOMEBUTTON_NAME {
                frontTouchedNode.run(scaleAction)
                moveToMenu()
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeUIActions()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeUIActions()
    }
    
    /*
     * Remove all driving actions and rescale button to normal size
     */
    func removeUIActions() {
        if !changingScene {
            let scaleAction = SKAction.scale(to: 1, duration: 0.1)
            drivingPedal?.run(scaleAction)
            breakPedal?.run(scaleAction)
            rewindButton?.run(scaleAction)
            homeButton?.run(scaleAction)
            car?.removeAllActions()
        }
    }
    
    // MARK: Scene changing
    func moveToMenu() {
        let transition:SKTransition = SKTransition.doorsOpenHorizontal(withDuration: 1.0)
        let scene:SKScene = SKScene(fileNamed: "MenuScene")!
        scene.scaleMode = SKSceneScaleMode.aspectFill
        self.view?.presentScene(scene, transition: transition)
    }
    
    func moveToWinningScene() {
        let transition:SKTransition = SKTransition.doorsOpenHorizontal(withDuration: 1.0)
        let scene:FinishScene = SKScene(fileNamed: "FinishScene") as! FinishScene
        scene.timeScore = currentDateInterval
        scene.scaleMode = SKSceneScaleMode.aspectFill
        self.view?.presentScene(scene, transition: transition)
    }
    
    func reloadScene() {
        let transition:SKTransition = SKTransition.fade(with: UIColor.red, duration: 1.0)
        
        var filename = "GameScene"
        if childNode(withName: "lamp1") != nil {
            filename.append("_night")
        }
        let scene:SKScene = SKScene(fileNamed: filename)!
        scene.scaleMode = SKSceneScaleMode.aspectFill
        self.view?.presentScene(scene, transition: transition)
    }
  
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if !changingScene, let startingTime = startTime {
            let now = Date()
            currentDateInterval = now.timeIntervalSince(startingTime)
            
            let minutes = floor(currentDateInterval/60)
            let minutesString = minuteFormatter.string(from: NSNumber(value: minutes))
            let seconds = Int(floor(currentDateInterval).truncatingRemainder(dividingBy: 60))
            let secondsString = minuteFormatter.string(from: NSNumber(value: seconds))
            let nanoseconds = Int((currentDateInterval - floor(currentDateInterval)) * 10000)
            let nanosecondString = nanosecondFormatter.string(from: NSNumber(value: nanoseconds))
            
            timer?.text = minutesString! + ":" + secondsString! + ":" + nanosecondString!
        }
    }
    
    //Takes all velocity from car
    func stopCar() {
        rearWheel?.removeAllActions()
        frontWheel?.removeAllActions()
        //Prevent the car from rolling back by applying a little force forward
        car?.physicsBody?.velocity = CGVector(dx: 15, dy: 0)
        frontWheel?.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        rearWheel?.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        frontWheel?.physicsBody?.angularVelocity = 0.0
        rearWheel?.physicsBody?.angularVelocity = 0.0
    }
  
    //Check for arriving the finishline
    override func didFinishUpdate() {
        if ((car?.position.x)! >= CGFloat(WINNING_DISTANCE)), !changingScene {
            stopCar()
            changingScene = true
            
            //Wait one second and then transition to the finish scene
            let action = SKAction.sequence([
                SKAction.wait(forDuration: 1),
                SKAction.run {
                    self.moveToWinningScene()
                }
                ])
            
            run(action)
        }
    }
}
