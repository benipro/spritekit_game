//
//  GameViewController.swift
//  SpriteKit Game
//
//  Created by Swift_entiwckler on 05.04.19.
//  Copyright © 2019 DHBW_Game_Production. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'MenuScene.sks'
            if let scene = SKScene(fileNamed: "MenuScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            // Variables to debug Physics and Performance
            //view.showsFPS = true
            view.showsPhysics = true
            //view.showsNodeCount = true
            //view.showsFields = true
        }
    }

    // Allow only landscape orientation and autorotate instantly
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return .landscape
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
