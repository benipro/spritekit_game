//
//  MenuScene.swift
//  SpriteKit Game
//
//  Created by Swift_entiwckler on 05.04.19.
//  Copyright © 2019 DHBW_Game_Production. All rights reserved.
//

//import Foundation
import SpriteKit

class MenuScene: SKScene {
    
    //Textures
    private let dayTexture = SKTexture(imageNamed: "sun")
    private let nightTexture = SKTexture(imageNamed: "mond")
    
    //Label that displays the GameType
    private var gameTypeLabel: SKLabelNode!
    
    //Nightmode variable
    private var nightMode = false
   
    
    override func didMove(to view: SKView) {
        gameTypeLabel = self.childNode(withName: "gameTypLabel") as? SKLabelNode
    }
    
    // MARK: Touch Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
                return
        }
        
        let location = touch.location(in: self)
        let frontTouchedNode = atPoint(location)
        
        //If the start button got pressed scale to 0.8
        if frontTouchedNode.name == "startButton" {
            if view != nil {
                let scaleAction =   SKAction.scale(to: 0.8, duration: 0.1)
                frontTouchedNode.run(scaleAction)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: self)
        let frontTouchedNode = atPoint(location)
        
        if view != nil {
            let scaleAction = SKAction.scale(to: 1, duration: 0.1)
            
            //If start Button got released scale back to normal for a smooth animation
            if frontTouchedNode.name == "startButton" {
                frontTouchedNode.run(scaleAction, completion: {
                    let transition:SKTransition = SKTransition.doorsOpenHorizontal(withDuration: 1.0)
                    var filename = "GameScene"
                    if self.nightMode {
                        filename.append("_night")
                    }
                    let scene:SKScene = SKScene(fileNamed: filename)!
                    scene.scaleMode = SKSceneScaleMode.aspectFill
                    self.view?.presentScene(scene, transition: transition)
                })
            
            } else {
                childNode(withName: "startButton")!.run(scaleAction)
            }
            
            //Buttons for choosing the Gametype
            if frontTouchedNode.name == "nightButton" {
                nightMode = !nightMode
                let widthSize = SKAction.resize(toWidth: 125, duration: 0)
                let heightSize = SKAction.resize(toHeight: 125, duration: 0)
                if(nightMode){
                    gameTypeLabel.text = "Night-Mode"
                    let action = SKAction.setTexture(nightTexture, resize: true)
                    frontTouchedNode.run(action)
                    frontTouchedNode.run(widthSize)
                    frontTouchedNode.run(heightSize)
                
                } else {
                    gameTypeLabel.text = "Day-Mode"
                    let action = SKAction.setTexture(dayTexture, resize: true)
                    frontTouchedNode.run(action)
                    frontTouchedNode.run(widthSize)
                    frontTouchedNode.run(heightSize)
                }
            }
        }
    }
}


