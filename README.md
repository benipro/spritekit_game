# Flat Racer
## Projekt von
Benjamin Seeger, Maximilian Rosnauer

## Installation
Das Projekt kann nach dem Herunterladen und Entpacken direkt gestartet werden.

## Benutzung und Test
Die App wurde auf dem iPhone X und auf dem iPad Pro (erste Generation) getestet. 
Die App sollte auf einem echten Gerät getestet werden, da der Simulator teilweise Probleme mit der Darstellung der
Texturen hat. (Zu großes Terrain für den Simulator, die Textur bleibt dann schwarz)

## Übersicht
Flat Racer ist ein Single-Player-Spiel Autorennspiel, das sowohl einen Tag- als auch einen Nachtmodus beinhaltet. Die unterschiedlichen Modi können im Startmenü, mit einem Tippen auf das Sonne/Mond-Icon, umgestellt werden. Das Ziel Spiels ist es, die Ziellinie in möglichst kurzer Zeit zu erreichen, um so den Highscore zu brechen. Bei einem Fehlversuch kann der Spieler über einen Tippen auf das Rückwärtssymbol (oben rechts in der Ecke) das Spiel neustarten. Über den Homebutton (links oben in der Ecke) gelangt der Spieler zurück in das Startmenü. Wenn die Ziellinie in erreicht wurde wird der Spieler auf die Abschlussszene weitergeleitet, in der er seine Zeit und den derzeitigen Highscore sehen kann. Bricht der Spieler den Highscore, wird diese Zeit als neuer Highscore übernommen und in Gold angezeigt. Über den Retrybutton gelangt er auf das Startmenü zurück. Im Nachmodus wird das Spiel "abgedunkelt" und Straßenlampen der Bildschirm wieder "aufgehellt".

=> Für die Implementierung des Spiels wurde das Framework: "SpriteKit" verwendet. 

## Implementierung
### Scene Files .sks
Über das Framework haben wir die unterschiedlichen Szenen erstellen und die nötigen Texturen hinein ziehen können. Die Szenen (GameScene, GameScene_night, FinishScene, MenuScene) wurden im SpriteKit-Scene Editor von XCode erstellt. Dort wurden alle Nodes hinzugefügt, positioniert und angepasst.
### Swift-Files .swift
Für jede Szene wurde ein ".swift"-File erstellt, dass die nötige Logik zur Szene enthält. Als Startscreen wird die MenuScene verwendet. Diese registirert "Touches" und leitet den Spieler daraufhin auf die GameScene weiter. 

In der GameScene kann das Fahrzeug über die Pedale gesteuert werden (Beschleunigen und Bremsen/Rückwärts). Außerdem wurden Flugzeuge eingebunden, die sich über den Bildschirm bewegen (Realisiert durch SKActions per Scene Builder). 

### Partikeleffekte
Über Partikelemitter (SKEmitterNode) wurden die Regen- und Schneeeffekte, sowie die Funken an der Zielflagge realisiert. Dazu wurde für jeden Emitter ein Particle File (.sks) erstellt. Die Partikelemitter werden in der GameScene.swift programmatisch hinzugefügt.

### Highscore
Ein Timer zeigt die vergangene Zeit seit dem ersten Tippen auf ein Pedal an. Beim Erreichen der Ziellinie wird der Timer gestoppt und bei der Transition zur FinishScene die gefahrene Zeit übergeben. In der FinishScene wird der gespeicherte Highscore übernommen.

### Prüfen der Ziellinie
Nach dem Rendern jeder Frame wird anhand der X-Koordinate des Fahrzeugs überprüft, ob die Ziellinie erreicht wurde. Ist dies der Fall, wird der Timer gestoppt. Das Fahrzeug wird angehalten und es wird ihm ein leichter Impuls nach vorne mitgegeben, um ein zurückrollen zu verhindern. Anschließend gibt es den Übergang zur FinishScene. 

### Physik
Die Physik wurde über die in SpriteKit integrierte Box2D-Physik-Engine realisiert. 
1. Die Physics-World wurde so konfiguriert, dass sich ein Gravitationseffekt von 9.81 einstellt. Dynamische Bodies mit der Property "affectedByGravity" werden nach unten hin beschleunigt.
1. Dem Terrain (groundNode) wurde ein statischer Physics-Body zugewiesen. Die Bounding-Box wurde über Alpha-Mapping realisiert, um eine genaue Kollisionen zu realisieren. 
2. Das Auto hat einen dynamischen Physicsbody, der von der Gravitation beeinflusst wird. Gleiches gilt für Vorder- und Hinterrad.
3. Das Auto ist mit jeweils 3 SKPhysicsJointSpring-Verbindungen mit den Rädern verbunden. Da eine Feder die Räder nicht in X-Richtung beschränkte, wurde das Rad durch 2 zusätzliche Federn nach vorne und hinten beschränkt. Die Verwendung von SKConstraints, um die Bewegung des Rades in Y-Richtung zu limitieren, stellte sich als problematisch heraus: Die Verwendung von SKConstraints in Kombination mit PhysicsBodies erzeugte Probleme in der Berechnung der Physik.
4. Beschleunigen/Bremsen wurde über das Anwenden von Drehimpulsen (Apply Angular Impulse) auf die Räder realisiert. Beide Räder werden angetrieben/gebremst.


### Szenenübergänge
Die Szenenübergänge wurden mit SKTransitions verschiedener Styles realisiert.

### Lichteffekte
Die Lichteffekte wurden mit SKLightNodes realisiert. Dazu wurden diese im SpritekKit-Scene-Editor hinzugefügt. Dort wurden auch für alle Entities, die von der Lichtquelle beeinflusst werden sollen, die passenden Light Bit Mask gesetzt.
Wählt der Spieler den Nachtmodus aus, wird die modifizierte "GameScene_night.sks" statt der "GameScene.sks" mit Tageslicht.



